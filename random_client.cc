#include <iostream>
#include <string>
#include <memory>

#include <cstdlib>

#include <grpcpp/grpcpp.h>

#include "random.pb.h"
#include "random.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using if4030::grpc::RandomService;
using if4030::grpc::SetBoundsRequest;
using if4030::grpc::SetBoundsResponse;
using if4030::grpc::NextRandomRequest;
using if4030::grpc::NextRandomResponse;

int main( int argc, char* argv[] ) {
    std::shared_ptr< Channel > channel = grpc::CreateChannel( "localhost:50051", grpc::InsecureChannelCredentials() );
    std::unique_ptr< RandomService::Stub > stub( RandomService::NewStub( channel ));
    
    if( argc > 2 ) {
        ClientContext context;
        SetBoundsRequest request;
        request.set_min( std::atoi( argv[1] ));
        request.set_max( std::atoi( argv[2] ));
        SetBoundsResponse response;
        Status status = stub->SetBounds( &context, request, &response );
        if( ! status.ok() ) {
            std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
        }
    }
    else {
        NextRandomRequest request;
        NextRandomResponse response;
        for (int i = 0; i < 10; ++i) {
            ClientContext context;
            Status status = stub->NextRandom( &context, request, &response );
            if( status.ok() ) {
                std::cout << response.random() << " ";
            }
            else {
                std::cout << status.error_code() << ": " << status.error_message()
                          << std::endl;
            }
        }
        std::cout << "\n";
    }

    return 0;
}

